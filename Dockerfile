FROM rust

# install rustfmt and clippy
RUN rustup component add rustfmt
RUN rustup component add clippy

# install musl tools and openssl
RUN apt update && \
    apt install -y \
        build-essential \
        pkg-config \
        cmake \
        libssl-dev \
        musl-dev \
        musl-tools && \
    rm -rf /var/lib/apt/lists/*

# add musl target
RUN rustup target add x86_64-unknown-linux-musl
RUN rustup target add wasm32-wasi
